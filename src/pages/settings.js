/**
 * * Author: Song
 */
import React from "react"
import Button from "@material-ui/core/Button"
import Card from "@material-ui/core/Card"
import Selectbar from "../components/settings/Selectbar"
import TextField from "@material-ui/core/TextField"
import Person from "./Person"
import EditProfile from "./EditProfile"
import Privacy from "./Privacy"
import DeleteAccount from "./DeleteAccount"
import { Router } from "@reach/router"
import picture1 from "../components/image/picture1.jpg"

import Layout from "../components/layout/Layout"

const SettingsPage = () => (
  <Layout>
    <div>
      <Selectbar path="settings" />
      <Router>
        <Person path="Person" />
        <EditProfile path="EditProfile" />
        <Privacy path="Privacy" />
        <DeleteAccount path="DeleteAccount" />
      </Router>
    </div>
  </Layout>
)

export default SettingsPage
