import React from "react"
import Layout from "../components/layout/Layout"
import styles from "./404.module.css"
import Typography from "@material-ui/core/Typography"
// Icons
import { GiHammerNails as Hammer } from "react-icons/gi"

const UnfinishedPage = () => (
  <Layout>
    <div className={styles.container}>
      <Hammer className={styles.logo} />
      <Typography variant="h2">
        This page is still under construction
      </Typography>
    </div>
  </Layout>
)

export default UnfinishedPage
