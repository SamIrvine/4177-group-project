import React, { Component } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Paper from "@material-ui/core/Paper"
import Grid from "@material-ui/core/Grid"
import Layout from "../components/layout/Layout"
import SelectBar from "../components/settings/Selectbar"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}))

export default function CenteredGrid() {
  const classes = useStyles()

  return (
    <Layout>
      <SelectBar />
      <div className={classes.root}>
        <div>
          <h1>Privacy note</h1>
        </div>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <h2>Account Privacy</h2>
              <p>
                When your account is private, only people you approve can see
                your photos and videos on Instagram. Your existing followers
                won't be affected.
              </p>
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <h2>Activity Status</h2>
              <p>
                Allow accounts you follow and anyone you message to see when you
                were last active on Instagram apps. When this is turned off, you
                won't be able to see the activity status of other accounts.
              </p>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </Layout>
  )
}
