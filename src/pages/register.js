import React, { Component } from "react"
import "./register.css"
import { Redirect } from "react-router-dom"
import Navigation from "../components/layout/Header.js"
import Typography from "@material-ui/core/Typography"
import FormControl from "@material-ui/core/FormControl"
import Input from "@material-ui/core/Input"
import InputLabel from "@material-ui/core/InputLabel"
import Grid from "@material-ui/core/Grid"
import Button from "@material-ui/core/Button"

const startState = {
  firstName: "",
  lastName: "",
  email: "",
  username: "",
  password: "",
  firstNameError: "",
  lastNameError: "",
  emailError: "",
  usernameError: "",
  passwordError: "",
  redirect: "/profile",
}
class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      username: "",
      password: "",
      firstNameError: "",
      lastNameError: "",
      emailError: "",
      usernameError: "",
      passwordError: "",
      redirect: null,
    }
  }
  handleInformation = newUser => {
    this.setState({
      [newUser.target.id]: newUser.target.value,
    })
  }
  handleRegistration = newUser => {
    newUser.preventDefault()
    const isValid = this.validateRegistrationForm()
    if (isValid) {
      console.log(this.state)
      this.setState(startState)
    }
  }
  validateRegistrationForm = () => {
    let firstNameError = ""
    let lastNameError = ""
    let emailError = ""
    let usernameError = ""
    let passwordError = ""

    if (this.state.firstName.length === 0) {
      firstNameError = "Required"
    }
    if (this.state.lastName.length === 0) {
      lastNameError = "Required"
    }
    if (!this.state.email.includes("@")) {
      emailError = "Invalid - Email should contain @"
    }
    if (this.state.username.length === 0) {
      usernameError = "Required"
    }
    if (this.state.password.length < 6) {
      passwordError = "Invalid - Password must contain 6 letters or more"
    }

    if (
      firstNameError ||
      lastNameError ||
      usernameError ||
      emailError ||
      passwordError
    ) {
      this.setState({ firstNameError })
      this.setState({ lastNameError })
      this.setState({ emailError })
      this.setState({ usernameError })
      this.setState({ passwordError })
      return false
    }

    return true
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    return (
      <div className="registerPage">
        {/* REGISTER OVERLAY */}
        <div
          className="imageHeader"
          style={{
            top: "0",
            position: "sticky",
            height: "64px",
            backgroundColor: "#f48fb1",
          }}
        ></div>
        {/* NAVIGATION HEADER */}
        <Navigation />

        {/* REGISTER BODY */}
        <Grid
          className="register"
          container
          direction="column"
          alignItems="center"
          justify="center"
          style={{ paddingTop: "75px" }}
        >
          <Typography
            xs={12}
            variant="h5"
            gutterBottom
            style={{
              paddingRight: "10px",
              paddingLeft: "10px",
              textAlign: "center",
            }}
          >
            To continue, please enter your information to NutriBoom
          </Typography>
          <form
            style={{ width: "75%", paddingTop: "20px", textAlign: "center" }}
            onSubmit={this.handleRegistration}
            autoComplete="off"
          >
            <FormControl style={{ width: "55%" }}>
              <div style={{ paddingBottom: "25px", width: "100%" }}>
                <InputLabel htmlFor="firstName">First Name</InputLabel>
                <Input
                  style={{ width: "100%" }}
                  placeholder="First Name"
                  type="text"
                  id="firstName"
                  onChange={this.handleInformation}
                  value={this.state.firstName}
                />
                <div style={{ fontSize: "10px", color: "red" }}>
                  <Typography
                    xs={12}
                    gutterBottom
                    style={{ paddingTop: "5px", textAlign: "center" }}
                  >
                    {this.state.firstNameError}
                  </Typography>
                </div>
              </div>
            </FormControl>
            <FormControl style={{ width: "55%" }}>
              <div style={{ paddingBottom: "25px", width: "100%" }}>
                <InputLabel htmlFor="lastName">Last Name</InputLabel>
                <Input
                  style={{ width: "100%" }}
                  placeholder="Last Name"
                  type="text"
                  id="lastName"
                  onChange={this.handleInformation}
                  value={this.state.lastName}
                />
                <div style={{ fontSize: "10px", color: "red" }}>
                  <Typography
                    xs={12}
                    gutterBottom
                    style={{ paddingTop: "5px", textAlign: "center" }}
                  >
                    {this.state.lastNameError}
                  </Typography>
                </div>
              </div>
            </FormControl>
            <FormControl style={{ width: "55%" }}>
              <div style={{ paddingBottom: "25px", width: "100%" }}>
                <InputLabel htmlFor="email">Email</InputLabel>
                <Input
                  style={{ width: "100%" }}
                  placeholder="Email"
                  type="email"
                  id="email"
                  onChange={this.handleInformation}
                  value={this.state.email}
                />
                <div style={{ fontSize: "10px", color: "red" }}>
                  <Typography
                    xs={12}
                    gutterBottom
                    style={{ paddingTop: "5px", textAlign: "center" }}
                  >
                    {this.state.emailError}
                  </Typography>
                </div>
              </div>
            </FormControl>
            <FormControl style={{ width: "55%" }}>
              <div style={{ paddingBottom: "25px", width: "100%" }}>
                <InputLabel htmlFor="username">Username</InputLabel>
                <Input
                  style={{ width: "100%" }}
                  placeholder="Username"
                  type="text"
                  id="username"
                  onChange={this.handleInformation}
                  value={this.state.username}
                />
                <div style={{ fontSize: "10px", color: "red" }}>
                  <Typography
                    xs={12}
                    gutterBottom
                    style={{ paddingTop: "5px", textAlign: "center" }}
                  >
                    {this.state.usernameError}
                  </Typography>
                </div>
              </div>
            </FormControl>
            <FormControl style={{ width: "55%" }}>
              <div style={{ paddingBottom: "25px", width: "100%" }}>
                <InputLabel htmlFor="password">Password</InputLabel>
                <Input
                  style={{ width: "100%" }}
                  placeholder="Password"
                  type="password"
                  id="password"
                  onChange={this.handleInformation}
                  value={this.state.password}
                />
                <div style={{ color: "red" }}>
                  <Typography
                    xs={12}
                    gutterBottom
                    style={{ paddingTop: "5px", textAlign: "center" }}
                  >
                    {this.state.passwordError}
                  </Typography>
                </div>
              </div>
              <Button
                type="submit"
                style={{ color: "#f48fb1", backgroundColor: "black" }}
                color="primary"
              >
                Register
              </Button>
            </FormControl>
          </form>
        </Grid>
      </div>
    )
  }
}

export default Register
