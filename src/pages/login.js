import React, { Component } from "react"
import "./login.css"
import { Redirect } from "react-router-dom"
import Navigation from "../components/layout/Header.js"
import Typography from "@material-ui/core/Typography"
import FormControl from "@material-ui/core/FormControl"
import Input from "@material-ui/core/Input"
import InputLabel from "@material-ui/core/InputLabel"
import Grid from "@material-ui/core/Grid"
import Button from "@material-ui/core/Button"

const startState = {
  email: "",
  password: "",
  emailError: "",
  passwordError: "",
  redirect: "/profile",
}

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: "",
      emailError: "",
      passwordError: "",
      redirect: null,
    }
  }
  handleAuthentification = user => {
    this.setState({
      [user.target.id]: user.target.value,
    })
  }
  handleSignIn = user => {
    user.preventDefault()
    const isValid = this.validateLoginForm()
    if (isValid) {
      console.log(this.state)
      this.setState(startState)
    }
  }
  validateLoginForm = () => {
    let emailError = ""
    let passwordError = ""

    if (!this.state.email.includes("@")) {
      emailError = "Invalid - Email should contain @"
    }
    if (this.state.password.length < 6) {
      passwordError = "Invalid - Password always contain 6 letters or more"
    }
    if (emailError || passwordError) {
      this.setState({ emailError })
      this.setState({ passwordError })
      return false
    }

    return true
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    return (
      <div className="loginPage">
        {/* LOGIN OVERLAY */}
        <div
          className="imageHeader"
          style={{ height: "64px", backgroundColor: "#f48fb1" }}
        ></div>
        {/* NAVIGATION HEADER */}
        <Navigation />

        {/* LOGIN BODY */}
        <Grid
          className="login"
          container
          direction="column"
          alignItems="center"
          justify="center"
          style={{ paddingTop: "75px" }}
        >
          <Typography
            xs={12}
            variant="h4"
            gutterBottom
            style={{
              paddingTop: "25px",
              paddingRight: "10px",
              paddingLeft: "10px",
              textAlign: "center",
            }}
          >
            To continue, please log in to NutriBoom
          </Typography>
          <form
            style={{ width: "75%", paddingTop: "20px", textAlign: "center" }}
            onSubmit={this.handleSignIn}
            autoComplete="off"
          >
            <FormControl style={{ width: "55%" }}>
              <div style={{ paddingBottom: "50px", width: "100%" }}>
                <InputLabel htmlFor="email">Email</InputLabel>
                <Input
                  style={{ width: "100%" }}
                  placeholder="Email"
                  type="email"
                  id="email"
                  onChange={this.handleAuthentification}
                  value={this.state.email}
                />
                <div style={{ fontSize: "10px", color: "red" }}>
                  <Typography
                    xs={12}
                    gutterBottom
                    style={{ paddingTop: "5px", textAlign: "center" }}
                  >
                    {this.state.emailError}
                  </Typography>
                </div>
              </div>
            </FormControl>
            <FormControl style={{ width: "55%" }}>
              <div style={{ paddingBottom: "50px", width: "100%" }}>
                <InputLabel htmlFor="password">Password</InputLabel>
                <Input
                  style={{ width: "100%" }}
                  placeholder="Password"
                  type="password"
                  id="password"
                  onChange={this.handleAuthentification}
                  value={this.state.password}
                />
                <div style={{ color: "red" }}>
                  <Typography
                    xs={12}
                    gutterBottom
                    style={{ paddingTop: "5px", textAlign: "center" }}
                  >
                    {this.state.passwordError}
                  </Typography>
                </div>
              </div>
              <Button
                type="submit"
                style={{ color: "#f48fb1", backgroundColor: "black" }}
                color="primary"
              >
                Sign In
              </Button>
            </FormControl>
          </form>
          <Typography
            xs={12}
            gutterBottom
            style={{
              paddingTop: "25px",
              paddingBottom: "5px",
              textAlign: "center",
            }}
          >
            <a href="#" className="loginFooter" style={{ color: "#f48fb1" }}>
              Forgot your password?
            </a>{" "}
            NutriBoom Can help!
          </Typography>
        </Grid>
      </div>
    )
  }
}

export default Login
