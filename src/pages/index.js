import React from "react"
import Landing from "./landing/landing.js"
import "./index.css"

const IndexPage = () => (
  <div>
    <Landing />
  </div>
)

export default IndexPage
