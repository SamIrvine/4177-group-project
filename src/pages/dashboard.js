/**
 * This is the starting point for authenticated routes, you can acess any page from the dashboard
 * @author Sam Irvine
 */

import React from "react"
import Layout from "../components/layout/Layout"
import styles from "./dashboard.module.css"
// Material Components
import { Typography, List } from "@material-ui/core/"
// Components
import DashboardCard from "../components/common/DashboardCard"
// Icons
import { GiMoneyStack as Budget, GiCookingPot as Fridge } from "react-icons/gi"
import { MdLocalGroceryStore as Lists } from "react-icons/md"
import {
  FaNutritionix as Nutrition,
  FaUserNinja as Profile,
  FaSlideshare as Sharing,
} from "react-icons/fa"

const Dashboard = () => {
  return (
    <Layout>
      <div className={styles.container}>
        <Typography className={styles.title} variant={"h2"}>
          Your Dashboard
        </Typography>

        <div className={styles.cardContainer}>
          <DashboardCard
            title="Budget"
            icon={<Budget className={styles.icon} />}
            description="Access Your Budget, Set Goals, Improve"
          />
          <DashboardCard
            title="Fridge"
            icon={<Fridge className={styles.icon} />}
            description="Check Whats in Your Fridge, Time to get Cooking"
            to="/Fridge/FridgePage"
            status="Live!"
          />
          <DashboardCard
            title="Lists"
            icon={<Lists className={styles.icon} />}
            description="Make A List, Check it, Get More Done"
          />
          <DashboardCard
            title="Nutrition"
            icon={<Nutrition className={styles.icon} />}
            description="Track Your Nutrition, Be The Best You"
          />
          <DashboardCard
            title="Profile"
            icon={<Profile className={styles.icon} />}
            description="View Your Profile"
            to="/profile/ProfilePage"
            status="Live!"
          />
          <DashboardCard
            title="Sharing"
            icon={<Sharing className={styles.icon} />}
            description="Share Things With Your Friends"
          />
        </div>
      </div>
    </Layout>
  )
}

export default Dashboard
