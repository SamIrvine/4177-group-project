/**
 * Landing Page
 * This is the home of the applciation when a user is not registered or logged in
 * Author: Jason Lalonde
 **/

import React from "react"
import "./landing.css"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import Navigation from "../../components/layout/navi.js"
import LandingFooter from "../../components/layout/footer.js"
import LandingCard from "../../components/layout/mainCard.js"

import Header from "../../components/layout/Header"

const LandingPage = () => (
  <div className="" style={{ width: "100%" }}>
    {/* LANDING IMAGE (Account or Name / Description / Date Published / Site Name / URL) 
    
        An, Min. Woman Sitting on Brown Stone Near Green Leaf Trees at Daytime. February 6, 2018. 
            Pexels. https://images.pexels.com/photos/1234035/pexels-photo-1234035.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260.

        Accessed Feb 17, 2020
    */}
    <div className="imageHeader" style={{ height: "70%" }}>
      <img
        src="https://images.pexels.com/photos/1234035/pexels-photo-1234035.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
        alt=""
        style={{ width: "100%" }}
      ></img>
    </div>

    {/* LANDING NAVIGATION */}
    <div className="container">
      <Header />
      <div
        className="container"
        style={{ width: "100%", height: "100%", color: "white" }}
      >
        <div className="information">
          <Typography
            className="info"
            variant="h2"
            style={{ color: "#f48fb1" }}
          >
            NutriBoom
          </Typography>
          <Typography
            className="info"
            variant="h6"
            style={{ paddingBottom: "15px" }}
          >
            NutriBoom is a sparkplug to the way you live. Nutriboom tracks your
            health all the way to expenses. Its a great way to keep up-to-date
            with everything you, with the option to spicy things up with fresh
            and creative recipes.
          </Typography>
          <Button
            className="info"
            style={{ color: "black", backgroundColor: "#f48fb1" }}
            color="primary"
          >
            Get Started
          </Button>
        </div>
      </div>
    </div>

    {/* LANDING CARD */}
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        marginRight: "30px",
        marginLeft: "30px",
      }}
    >
      <LandingCard />
    </div>

    {/* LANDING FOOTER */}
    <LandingFooter />
  </div>
)

export default LandingPage
