import React, { Component } from "react"
import TextField from "@material-ui/core/TextField"
import { makeStyles } from "@material-ui/core/styles"
import Button from "@material-ui/core/Button"
import Layout from "../components/layout/Layout"
import SelectBar from "../components/settings/Selectbar"

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    marginLeft: 300,
  },
  textField: {
    marginLeft: 0,
    marginRight: theme.spacing(1),
    width: 200,
  },
  input: {
    display: "none",
  },
}))

export default function DeleteAccount() {
  const classes = useStyles()
  return (
    <Layout>
      <SelectBar />
      <form className={classes.root} noValidate autoComplete="off">
        <div>
          <h1>Wanna delete your account?</h1>
          <div>
            <TextField
              id="filled-textarea"
              label="User name"
              placeholder="Username"
              multiline
              variant="filled"
            />
          </div>
          <div>
            <TextField
              id="filled-textarea"
              label="Password"
              placeholder="Password"
              multiline
              variant="filled"
            />
          </div>
          <div>
            <TextField
              id="filled-textarea"
              label="Email"
              placeholder="Email"
              multiline
              variant="filled"
            />
          </div>
          <div>
            <TextField
              id="filled-textarea"
              label="Phone number"
              placeholder="Phonenumber"
              multiline
              variant="filled"
            />
          </div>
          <div>
            <Button variant="contained" color="primary" disableElevation>
              Submit
            </Button>
          </div>
        </div>
      </form>
    </Layout>
  )
}
