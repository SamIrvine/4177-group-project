import React from "react"
import { TextField, makeStyles, Button, Grid } from "@material-ui/core"

const useStyles = makeStyles(theme => ({
  forms: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 300,
    },
  },
}))

function AboutUser() {
  const classes = useStyles()

  return (
    <React.Fragment>
      <Grid
        item
        xs
        direction="row"
        justify="space-between"
        alignItems="flex-start"
      >
        <form className={classes.forms} noValidate autoComplete="off">
          <TextField
            id="first-name"
            label="First Name"
            palceholder="First Name"
            defaultValue="Ze "
            disabled
          />
          <TextField
            id="last-name"
            label="Last Name"
            placeholder="Last Name"
            defaultValue="Bosc"
            disabled
          />

          <br />
          <TextField
            required
            id="standard-email-input"
            label="E-mail"
            type="email"
            defaultValue="example@nutriboom.ca"
            disabled
          />
          <br />
          <br />
          <TextField
            required
            id="standard-username-input"
            label="Username"
            defaultValue="Nutri Boomer"
            disabled
          />
          <br />
          <TextField
            required
            id="standard-password-input"
            label="Password"
            defaultValue=" "
            type="password"
            autoComplete="current-password"
            disabled
          />

          <br />
          <TextField
            id="standard-aboutme-text"
            label="About me"
            multiline
            defaultValue=" "
            rows="2"
            placeholder="Tell us more about yourself!"
            disabled
          />
          <br />
          <br />
          <Button
            variant="outlined"
            color="primary"
            href="/profile/ProfilePage#nicebrah"
          >
            Edit Profile
          </Button>
        </form>
      </Grid>
    </React.Fragment>
  )
}

export default AboutUser
