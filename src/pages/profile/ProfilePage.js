/**
 * TODO Create the landing page
 * This is the starting pint for our web application, our landing page will start from here.
 * * Author: Emad B.
 */
import React from "react"
import {
  makeStyles,
  Paper,
  Grid,
  Container,
  Typography,
} from "@material-ui/core/"
import Navbar from "../../components/layout/Header"
import AboutUser from "./AboutUser"
import FriendList from "./FriendList"
import FavoriteRecipes from "./FavoriteRecipes"
import ProfileImage from "../../components/playground/ProfileImage"
import Layout from "../../components/layout/Layout"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.primary,
  },
}))

export default function CenteredGrid() {
  const classes = useStyles()

  return (
    <Layout>
      <Container fixed>
        <br />
        <Typography variant="h5" gutterBottom>
          My Information
        </Typography>
        <br />

        <Grid container spacing={3}>
          <Grid item xs>
            <Paper className={classes.paper}>
              <AboutUser />
            </Paper>
          </Grid>
          <Grid item xs={3}>
            <ProfileImage />
            <br />
            <FriendList />
          </Grid>
          <br />
          <Grid item xs={9}>
            <Paper className={classes.paper}>
              <FavoriteRecipes />
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  )
}
