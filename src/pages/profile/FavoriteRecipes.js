import React from "react"
import { Typography } from "@material-ui/core"
import RecipeCard from "../../components/playground/RecipeCard"

function FavoriteRecipes() {
  return (
    <React.Fragment>
      <Typography variant="h5" gutterBottom>
        My Favorite(s)
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        Recipes:
      </Typography>
      <RecipeCard />
    </React.Fragment>
  )
}

export default FavoriteRecipes
