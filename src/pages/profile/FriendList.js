import React from "react"
import {
  Paper,
  Grid,
  Avatar,
  Typography,
  makeStyles,
  Button,
} from "@material-ui/core/"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
  },
  button: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: "blue",
  },
}))

//const classes = useStyles()

function FriendList() {
  const friendInfo = "First, Last Name(s)"
  const classes = useStyles()

  return (
    <React.Fragment>
      {/* Correct me cuz I'm wrong =) */}
      <Typography variant="h5" gutterBottom>
        Friend List
      </Typography>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item>
            <Avatar> A</Avatar>
          </Grid>
          <Grid item xs zeroMinWidth>
            <Typography noWrap>{friendInfo}</Typography>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item>
            <Avatar>EB</Avatar>
          </Grid>
          <Grid item xs zeroMinWidth>
            <Typography noWrap>Emad B.</Typography>
          </Grid>
        </Grid>
      </Paper>
      {/* Correct me cuz I'm wrong =) */}
      <br />
      <Button
        color="inherit"
        href="/profile/ProfilePage#coolbrah"
        className={classes.button}
      >
        Add a friend
      </Button>
    </React.Fragment>
  )
}

export default FriendList
