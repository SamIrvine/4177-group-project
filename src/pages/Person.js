import React, { Component } from "react"
import { makeStyles } from "@material-ui/core/styles"
import TextField from "@material-ui/core/TextField"
import Layout from "../components/layout/Layout"
import SelectBar from "../components/settings/Selectbar"

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    marginLeft: 200,
  },
  textField: {
    marginLeft: 0,
    marginRight: theme.spacing(1),
    width: 200,
  },
}))

export default function LayoutTextFields() {
  const classes = useStyles()

  return (
    <Layout>
      <SelectBar />
      <div className={classes.root}>
        <h1>Personal information</h1>

        <div>
          <TextField
            id="filled-full-width"
            label="Label"
            style={{ margin: 8 }}
            placeholder="Full address"
            helperText="Full width!"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="filled"
          />
          <TextField
            label="First Nmae"
            id="filled-margin-none"
            defaultValue=""
            className={classes.textField}
            helperText="Some important text"
            variant="filled"
          />
          <TextField
            label="Last Name"
            id="filled-margin-dense"
            defaultValue=""
            className={classes.textField}
            helperText="Some important text"
            margin="dense"
            variant="filled"
          />
          <TextField
            label="Full name"
            id="filled-margin-normal"
            defaultValue=""
            className={classes.textField}
            helperText="Some important text"
            margin="normal"
            variant="filled"
          />
        </div>
        <div>
          <TextField
            id="outlined-full-width"
            label="Email"
            style={{ margin: 8 }}
            placeholder="Email"
            helperText="Full width!"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          />
          <TextField
            label="Phone number"
            id="outlined-margin-none"
            defaultValue=""
            className={classes.textField}
            helperText="Some important text"
            variant="outlined"
          />
          <TextField
            label="City"
            id="outlined-margin-dense"
            defaultValue=""
            className={classes.textField}
            helperText="Some important text"
            margin="dense"
            variant="outlined"
          />
          <TextField
            label="Postcode"
            id="outlined-margin-normal"
            defaultValue=""
            className={classes.textField}
            helperText="Some important text"
            margin="normal"
            variant="outlined"
          />
        </div>
      </div>
    </Layout>
  )
}
