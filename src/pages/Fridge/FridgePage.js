/**
 * @author Fahad ALFaleh
 * @date Feb/10/2020
 */
import React, { Component } from "react"
import Recipe from "./Recipe"
import Layout from "../../components/layout/Layout"

class FridgePage extends Component {
  render() {
    return (
      <Layout>
        <Recipe />
      </Layout>
    )
  }
}

export default FridgePage
