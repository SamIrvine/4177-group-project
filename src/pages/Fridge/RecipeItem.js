/**
 * @author Fahad ALFaleh
 * @date Feb/10/2020
 */
import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import { CardMedia, CardContent, Typography, Box } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import IconButton from '@material-ui/core/IconButton';

class RecipeItem extends Component {
	render() {
		const div = {
			'overflow-y': 'scroll',
			'max-height': '350px'
		};
		const root = {
			maxWidth: 350
		};
		const media = {
			height: '0',
			paddingTop: '56.25%', // 16:9,
			marginTop: '30'
		};

		let { item } = this.props;

		if (item === null) {
			item = { recipe: '', image: '', title: '' };
		}

		return (
			<Grid item xs={12} sm={6} md={4} justify="center" container>
				<Card style={root}>
					<CardMedia style={media} image={item || {}.image} alt="Test" />
					<CardContent>
						<Typography gutterBottom variant="body" component="h5">
							{item || {}.title}
						</Typography>
						<div style={div}>
							<Box>
								<Typography variant="body" color="textSecondary" component="p">
									{item || {}.recipe}
								</Typography>
							</Box>
						</div>
					</CardContent>
					<CardActions disableSpacing>
						<IconButton aria-label="add to favorites">
							<FavoriteIcon />
						</IconButton>
						<IconButton aria-label="share">
							<ShareIcon />
						</IconButton>
					</CardActions>
				</Card>
			</Grid>
		);
	}
}

export default RecipeItem;
