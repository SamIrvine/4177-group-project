/**
 * @author Fahad ALFaleh
 * @date Feb/10/2020
 */
import React, { Component } from 'react';
import axios from 'axios';
import RecipeItem from './RecipeItem';
import { Container, Grid } from '@material-ui/core';

class Recipes extends Component {
	constructor() {
		super();
		this.state = {
			items: []
		};
	}

	render() {
		return (
			<Container>
				<h2>Recipe page</h2>
				<Grid container justify="center" spacing={2}>
					{this.state.items.map((value, index) => {
						return <RecipeItem key={index} item={value} />;
					})}
				</Grid>
			</Container>
		);
	}

	componentDidMount() {
		axios
			.get('https://api.spoonacular.com/recipes/random?number=6&apiKey=701f2d033df04606802cc878fb3e564e')
			.then((response) => {
				let data = response.data.recipes;

				const recipes = [];
				for (let recipe in data) {
					if (data.hasOwnProperty(recipe)) {
						let recipeItem = [];
						recipeItem['title'] = data[recipe]['title'];
						recipeItem['recipe'] = data[recipe]['instructions'];
						recipeItem['image'] = data[recipe]['image'];
						recipes.push(recipeItem);
					}
				}
				this.setState({
					items: recipes
				});

				console.log(this.state);
			});
	}
}

export default Recipes;
