import React, { Component } from "react"
import Button from "@material-ui/core/Button"
import ButtonGroup from "@material-ui/core/ButtonGroup"
import { makeStyles } from "@material-ui/core/styles"
import TextField from "@material-ui/core/TextField"
import { Link } from "gatsby"

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}))
export default function LayoutTextFields() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <ButtonGroup
        orientation="vertical"
        color="primary"
        aria-label="vertical contained primary button group"
        variant="contained"
      >
        <Link to="Person">
          <Button>Personal info</Button>
        </Link>
        <Link to="EditProfile">
          <Button>Edit Profile</Button>
        </Link>
        <Link to="Privacy">
          <Button>Privacy</Button>
        </Link>
        <Link to="DeleteAccount">
          <Button>DeleteAccount</Button>
        </Link>
      </ButtonGroup>
    </div>
  )
}
