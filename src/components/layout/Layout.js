/**
 * TODO Add a consistent header and footer component to the layout
 * @description Component to be added to pages, contains constant footer and header components
 * @param children: The body of the layout component
 * * Author: Sam Irvine
 **/
import React from "react"
import PropTypes from "prop-types"

import Header from "./Header"

// ! The sites global CSS file, even though it is not used it needs to be imported into this file for global stylings to take affect
import global from "./global.css"

const Layout = ({ children }) => {
  const url = typeof window !== "undefined" ? window.location.href : ""
  return (
    <div>
      <Header loggedIn={url === "/" ? false : true} />
      <div className={"content"}>{children}</div>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
