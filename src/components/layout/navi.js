import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";

class Navigation extends Component {
    // Beginning background state
    state = {
        backgroundColor: 'transparent'
    }
    
    // Navbar is transparent until you scoll past 100px
    scrollingListener = e => {
        if (window.scrollY >= 0 && window.scrollY <= 100) {
          this.setState({backgroundColor: 'transparent'})
        } else {
          this.setState({backgroundColor: '#f48fb1'})
        }
    }
    
    // Event listener gets called whenever user scrolls
    componentDidMount() {
        window.addEventListener('scroll', this.scrollingListener)
    }
    
    render() {
        return( 
            <React.Fragment>
                <AppBar position="fixed" style={{backgroundColor: this.state.backgroundColor}} elevation={0}>
                    {/* Navbar component - Currently not changeable but can be if needed for other pages */}
                    <Toolbar>
                        <div className="leftNav" style={{flexGrow: "1", borderRadius: "16px"}}>
                            <Button color="inherit">
                                <Typography variant="h6">
                                    Nutriboom
                                </Typography>
                            </Button>
                        </div>
                        <div className="rightNav">
                            <Button color="inherit" style={{borderRadius: "16px"}}>Tutorial</Button>
                            <Button color="inherit" style={{borderRadius: "16px"}}>About</Button>
                            <Button color="inherit" style={{borderRadius: "16px"}}>Login</Button>
                            <Button color="inherit" style={{borderRadius: "16px", color: "black", backgroundColor: "#f48fb1"}}>Register</Button>
                        </div>
                    </Toolbar>
                </AppBar>  
            </React.Fragment>
        )
    }
}
 
export default Navigation;