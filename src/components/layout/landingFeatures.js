import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from "@material-ui/core/Button";
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import ChatBubbleRoundedIcon from '@material-ui/icons/ChatBubbleRounded';
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import ListAltRoundedIcon from '@material-ui/icons/ListAltRounded';
import FavoriteRoundedIcon from '@material-ui/icons/FavoriteRounded';
import NotificationsRoundedIcon from '@material-ui/icons/NotificationsRounded';
import SettingsRoundedIcon from '@material-ui/icons/SettingsRounded';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';

const LandingFeatures = props => {
    return (
        <div style={{paddingBottom: "50px"}}>
        <div className="landingFeatures" style={{width: "100%", height: "20px", borderBottom: "1px solid black", textAlign: "center"}}>
          <div className="hrPadding">
          <Typography variant="h4" style={{backgroundColor: "white",color: "#f48fb1"}}>
            Other Features
          </Typography>
          </div>
        </div>
        <div className="otherFeature" style={{paddingTop: "25px", height: "100%", width: "90%", textAlign: "center", display: "block", marginLeft: "auto", marginRight: "auto"}}>
          <Grid container spacing={6} style={{justifyContent: "center", height: "100%", alignItems:  "center", paddingTop: "100px"}}>
            <Grid item xs={12} sm={6} md={3}>
              <CheckCircleRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Goal Setting
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ChatBubbleRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Group Chat
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <SearchRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Browse NutriBoom
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ListAltRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Tasty Recipes 
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={6} style={{justifyContent: "center", height: "100%", alignItems:  "center", paddingTop: "50px"}}>
            <Grid item xs={12} sm={6} md={3}>
              <FavoriteRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Favourites
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <NotificationsRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Noticfications
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <AccountCircleRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Account Customization
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <SettingsRoundedIcon/>
              <Typography variant="h5" style={{ color: "#f48fb1"}}>
              Settings Feature
              </Typography>
            </Grid>
          </Grid>
          <Typography variant="h2" style={{paddingBottom: "25px", paddingTop: "100px"}}>
            Get Started with NutriBoom!
          </Typography>
          <Button style={{color: "black", backgroundColor: "#f48fb1"}}color="primary">BOOM BOOM Start!</Button>
        </div>
      </div>
    );
};
 
export default LandingFeatures;