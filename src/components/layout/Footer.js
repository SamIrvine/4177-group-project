// Footer component is only for landing page

import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { SocialIcon } from 'react-social-icons';

const Footer = props => {
  return (
    <div className="footer" style={{marginTop: "50px"}}> 

      {/* SOCILA MEDIA ICONS / TOP OF FOOTER 
      
          “React-Social-Icons.” npm. Accessed February 17, 2020. https://www.npmjs.com/package/react-social-icons.
          “The Fastest Way to Share a Moment!” Snapchat. Accessed February 17, 2020. https://www.snapchat.com/.
          “Instagram.” Instagram. Accessed February 17, 2020. https://www.instagram.com/.
          “Twitter. It's What's Happening.” Twitter. Twitter. Accessed February 17, 2020. https://twitter.com/.
          Facebook. Accessed February 17, 2020. https://www.facebook.com/.
      
      */}
      <div className="footerTop" style={{backgroundColor:"black", height: "80px"}}>
        <Typography variant="h6" style={{paddingLeft: "25px", paddingTop: "25px", float: "left", color: "#f48fb1"}}>Visit us on social media!</Typography>
        <div className="socialIcons" style={{float: "right", paddingRight: "25px", paddingTop: "15px"}}>
          <SocialIcon url="https://www.snapchat.com/" style={{marginRight: "5px"}}/>
          <SocialIcon url="https://www.instagram.com/?hl=en" style={{marginRight: "5px"}}/>
          <SocialIcon url="https://twitter.com/?lang=en" style={{marginRight: "5px"}}/>
          <SocialIcon url="https://www.facebook.com/"/>
        </div>
      </div>

      {/* COMPANY STATMENT / FOOTER LINKS / TERMS / BOTTOM OF FOOTER */}
      <div className="footerBottom" style={{width: "100%", height: "100%",  backgroundColor:"#f48fb1"}}>
        <div className="footerLinks" style={{width: "100%", height: "200px"}}>  
          
          {/* COMPANY STATEMENT */}
          <div className="footerLinksLeft" style={{float: "left", height: "100%"}}>
            <Typography className="footerLeftText" variant="h5" style={{paddingTop: "50px"}}><b>NUTRIBOOM</b></Typography>
            <p className="footerLeftText">Sparkplug to the way you live. Booming Possibilities.</p>
            <p className="footerLeftText"><b>Be Bold.</b></p>
          </div>

          {/* FOOTER LINKS */}
          <div className="footerLinksRight" style={{float: "right", height: "100%"}}>
            <Grid container spacing={2} style={{height: "100%"}}>
              <Grid item xs={4}>
                <Typography variant="h6" style={{float: "left", paddingTop: "50px", paddingLeft: "50px"}}><b>Explore</b></Typography>
                <ul className="footerLists" style={{listStyleType: "none", paddingTop: "75px", paddingLeft: "50px"}}>
                  <li>Tutorial</li>
                  <li>About</li>
                  <li>Get Started</li>
                </ul>
              </Grid>
              <Grid item xs={4}>
                <Typography variant="h6" style={{float: "left", paddingTop: "50px", paddingLeft: "50px"}}><b>Features</b></Typography>
                <ul className="footerLists" style={{listStyleType: "none", paddingTop: "75px", paddingLeft: "50px"}}>
                  <li>Health Tracking</li>
                  <li>Food Tracking</li>
                  <li>Expense Tracking</li>
                  <li>Other Features</li>
                </ul>
              </Grid>
              <Grid item xs={4}>
                <Typography variant="h6" style={{float: "left", paddingTop: "50px", paddingLeft: "50px"}}><b>Visit</b></Typography>
                <ul className="footerLists" style={{listStyleType: "none", paddingTop: "75px", paddingLeft: "50px"}}>
                  <li>1234 Cloud9 Street, CS Building, University of the Coolest</li>
                </ul>
              </Grid>
            </Grid>      
          </div>
        </div>
        
        {/* COPYRIGHT / TERMS */}
        <div className="footerTerms" style={{width: "100%", height: "20px"}}>
          <div className="footerTermsLeft" style={{paddingTop: "30px", float: "left", width: "50%", height: "100%"}}>
            <p style={{paddingTop: "20px", paddingLeft: "30px",float: "left"}}>&copy;2020 Enjoy the NutriBoom.</p>
          </div>
          <div className="footerTermsRight" style={{paddingTop: "30px", float: "right", width: "45%", height: "100%"}}>
            <p style={{paddingTop: "20px", paddingRight: "30px",float: "right"}}><a href="/" >Terms</a>&nbsp;&nbsp;&#9830;&nbsp;&nbsp;<a href="/" >Privacy</a>&nbsp;&nbsp;&#9830;&nbsp;&nbsp;<a href="/" >Conditions</a></p>
          </div>
        </div>
      </div>
    </div>
  );
};
 
export default Footer;