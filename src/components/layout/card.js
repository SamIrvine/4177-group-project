/*
 *
 * Card Component can be used for all types of pages
 * Currently has pre-set styles (we can change this)
 * takes 3 props (image, title, info)
 * View landing page to get a better understand
 * 
 * */

import React from "react";
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Card from "@material-ui/core/Card";

const CardComponent = props => {
    return (
        <Card style={{ height: "320px"}}>
            <CardActionArea>
                {/* IMAGE PROP GOES HERE */}
                <CardMedia style={{height: "200px"}} image={props.image}/>
                <CardContent>
                    {/* TITLE PROP GOES HERE */}
                    <Typography variant="h5" style={{color: "#f48fb1"}}>{props.title}</Typography>
                    {/* INFO PROP GOES HERE */}
                    <Typography variant="body2" color="textSecondary" component="p">{props.info}</Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};
 
export default CardComponent;