/**
 * @description Functional Header Component contains sitewide navigation, a logo, and profile (on authed routes)
 * @param loggedIn when the user is logged in set to true, conditionally render profile, logo click route to dashboard, else landing
 * @author Sam Irvine
 */

import { Link } from 'gatsby';
import React, { Component, Fragment } from 'react';
import styles from './Header.module.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core/';

// Icons
import { GiHamburgerMenu as Menu } from 'react-icons/gi';
import { AiOutlineClose as Close } from 'react-icons/ai';
import logo from '../../images/logo.png';

class Header extends Component {
	constructor(props) {
		super(props);

		this.state = { showDrawer: false };
	}

	componentDidMount() {
		window.addEventListener('resize', this.windowResize);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.windowResize);
	}

	toggleDrawer = () => {
		this.setState({ showDrawer: !this.state.showDrawer });
	};

	windowResize = () => {
		if (window.innerWidth > 768) {
			this.setState({ showDrawer: false });
		}
	};

	render() {
		const { loggedIn } = this.props;
		const { showDrawer } = this.state;

		return (
			<div className={styles.headerContainer}>
				<Link className={styles.title} to={loggedIn ? '/dashboard' : '/'}>
					<Typography variant="h1">NutriBoom</Typography>
				</Link>

				<div className={classNames(styles.menuContainer, styles.hamburgerContainer)}>
					<Menu onClick={this.toggleDrawer} className={styles.hamburgerIcon} />
				</div>

				<div className={classNames(styles.menuContainer, showDrawer && styles.drawer)}>
					{showDrawer && <Close onClick={showDrawer && this.toggleDrawer} className={styles.closeIcon} />}

					{loggedIn ? (
						<Fragment>
							<Link
								onClick={showDrawer && this.toggleDrawer}
								className={classNames(styles.firstButton, styles.headerButton)}
								to="/"
							>
								Landing
							</Link>
							<Link
								onClick={showDrawer && this.toggleDrawer}
								className={styles.headerButton}
								to="/dashboard"
							>
								Dashboard
							</Link>
							<Link
								onClick={showDrawer && this.toggleDrawer}
								className={styles.headerButton}
								to="/profile/ProfilePage"
							>
								Profile
							</Link>
							<Link
								onClick={showDrawer && this.toggleDrawer}
								className={styles.headerButton}
								to="/settings"
							>
								Settings
							</Link>
						</Fragment>
					) : (
						<Fragment>
							<Link
								onClick={showDrawer && this.toggleDrawer}
								className={classNames(styles.firstButton, styles.headerButton)}
								to="/login"
							>
								Login
							</Link>
							<Link
								onClick={showDrawer && this.toggleDrawer}
								className={styles.headerButton}
								to="/register"
							>
								Register
							</Link>
							<Link onClick={showDrawer && this.toggleDrawer} className={styles.headerButton} to="/About">
								About
							</Link>
							<Link
								onClick={showDrawer && this.toggleDrawer}
								className={styles.headerButton}
								to="/dashboard"
							>
								Dashboard
							</Link>
						</Fragment>
					)}

					{showDrawer && (
						<Link className={styles.drawerTitle} to={loggedIn ? '/dashboard' : '/'}>
							<Typography variant="h1">NutriBoom</Typography>
						</Link>
					)}
				</div>
			</div>
		);
	}
}

Header.propTypes = {
	loggedIn: PropTypes.bool
};

export default Header;
