/*
* Large card for the landing page
**/

import React from "react";
import Typography from '@material-ui/core/Typography';
import Card from "@material-ui/core/Card";
import Grid from '@material-ui/core/Grid';
import LandingFeatures from "./landingFeatures.js";
import LandingCard from "./card.js";

const MainCard = props => {
    return (
        <React.Fragment>
            <Card className="mainCard" xs={12} style={{ height: "100%", width: "190vh"}} raised>
                <Typography xs={12} variant="h2" gutterBottom style={{paddingTop: "75px", paddingRight: "10px", paddingLeft: "10px", textAlign: "center"}}>Why choose NutriBoom?</Typography>
                <Typography xs={12} variant="h6" gutterBottom style={{color: "#999", paddingTop: "15px", textAlign: "center"}} >NutriBoom is a sparkplug to the way you live. NutriBoom tracks your health all the way to expenses. Its a great way to keep up-to-date with everything you, with the option to spicy things up with fresh and creative recipes.</Typography>   
                <div className="landingCards" style={{paddingTop: "75px", height: "100%", width: "90%", textAlign: "center", display: "block", marginLeft: "auto", marginRight: "auto"}}>
                    <Grid container spacing={6} style={{height: "100%", alignItems:  "center", paddingBottom: "100px"}}>
                    <Grid item xs={12} md={4}>
                        {/* HEALTH CARD (Account or Name / Description / Date Published / Site Name / URL) 
                         
                            Chevanon, Photography. Woman With Arms Outstretched Against Blue Sky. February 6, 2017. 
                                Pexels. https://images.pexels.com/photos/317155/pexels-photo-317155.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500.

                            Accessed Feb 17, 2020
                         */}

                        <LandingCard 
                        title="Health Tracking"
                        info="NutriBoom tracks your health everyday. In order for you to meet any goal!"
                        image="https://images.pexels.com/photos/317155/pexels-photo-317155.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                        />
                    </Grid>
                    <Grid item xs={12} md={4}>
                        {/* FOOD CARD (Account or Name / Description / Date Published / Site Name / URL)  
                         
                            Olsson, Ella. Six Fruit Cereals in Clear Glass Mason Jars on White Surface. November 20, 2018. 
                                Pexels. https://images.pexels.com/photos/1640768/pexels-photo-1640768.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500.
                                
                            Accessed Feb 17, 2020
                         */}
                        <LandingCard 
                        title="Food Tracking"
                        info="NutriBoom tracks the food you have at home to help you find new and tasty recipes!"
                        image="https://images.pexels.com/photos/1640768/pexels-photo-1640768.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                        />
                    </Grid>
                    <Grid item xs={12} md={4}>
                        {/* EXPENSE CARD (Account or Name / Description / Date Published / Site Name / URL)  
                         
                            Lewis, Jessica. Gold Iphone 7 Beside Space Gray Macbook Pro. September 2, 2017. 
                                Pexels. https://images.pexels.com/photos/583846/pexels-photo-583846.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500.

                            Accessed Feb 17, 2020
                         */}
                        <LandingCard 
                        title="Expense Tracking"
                        info="NutriBoom tracks any food depts you may owe to friends and family, so that you never forget!"
                        image="https://images.pexels.com/photos/583846/pexels-photo-583846.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                        />
                    </Grid>
                    </Grid>
                </div>
                
                {/* OTHER FEATURES SECTION*/}
                <LandingFeatures/>
            </Card>
        </React.Fragment>
    );
};
export default MainCard;