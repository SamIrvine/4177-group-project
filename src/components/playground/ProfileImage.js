import React from "react"
import { makeStyles, CardMedia } from "@material-ui/core"
import Card from "@material-ui/core/Card"
import CardActions from "@material-ui/core/CardActions"
import Typography from "@material-ui/core/Typography"
import { Link } from "@material-ui/core"

const useStyles = makeStyles({
  card: {},
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
})

export default function OutlinedCard() {
  const classes = useStyles()

  return (
    <Card className={classes.card} variant="outlined">
      <CardMedia
        component="img"
        alt="Profile Picture"
        height="140"
        image="https://svgsilh.com/svg_v2/2494130.svg"
        title="Contemplative Reptile"
      />

      <CardActions>
        <Link href="/profile/ProfilePage#uploadimage">
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Upload Picture
          </Typography>
        </Link>
      </CardActions>
    </Card>
  )
}
