import React from "react"
import { Link } from "gatsby"
import styles from "./DashboardCard.module.css"

import { Typography } from "@material-ui/core/"

const DashboardCard = ({ title, icon, description, to, status }) => {
  return (
    <Link to={to ? to : "/dashboard"} className={styles.container}>
      <div>
        <Typography className={styles.title} variant="h2">
          {title}
        </Typography>
      </div>
      <div>{icon}</div>
      <div className={styles.descriptionContainer}>
        <Typography className={styles.description} variant="p">
          {description}
        </Typography>
      </div>
      <Typography
        className={status === "Live!" ? styles.status : styles.noStatus}
      >
        {status}
      </Typography>
    </Link>
  )
}

export default DashboardCard
