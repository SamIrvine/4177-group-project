# 4177 Group Project (Title TBD) 🚀🚀

## Site Url: https://4177project.netlify.com/

## Setup:

To start developing make sure you have node and the Gatsby CLI

In your terminal run: `npm install -g gatsby-cli`

After that you can clone this repo to your local machine

`cd` into the project folder

Run: `gatsby develop` to spin up the site on port 8000

If you get stuck you can reference the [Gatsby quickstart guide](https://www.gatsbyjs.org/docs/quick-start/)
(This also includes a link to a more verbose beginner tutorial)

## Making Changes:

When you are ready to work on a feature, make a branch starting from the **development** branch

Personally I like to create a feature branch for any individual components, naming it something like _feature-primaryButton_

When you complete a component/feature, make a pull request to merge your branch into development
(We'll point our hosting to the master branch so if we break something our website won't go down )

## Tech (subject to change)

### Libraries/Frameworks

For the purpose of the first iteration we'll be using [Gatsby](https://www.gatsbyjs.org/), this will allow us to make a front-end prototype quickly, this also gives us the benefit of re-using the react components in future iterations of our website.

### Design System

We will be using [materialize ui](https://material-ui.com/) so that we can have consistent styling throughout the application

## Team

- [Sam Irvine](sam.irvine@dal.ca) - (Developer)
- [Jason Lalonde](Jason.Lalonde@dal.ca) - (Developer)
- [Fahad ALFaleh](fahad.k.alfaleh@gmail.com) - (Developer)
- [Emad Bamatraf](em392020@dal.ca) - (Developer)
- [Name](email@dal.ca) - (Role)
